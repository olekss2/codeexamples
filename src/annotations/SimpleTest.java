package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.Repeatable;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

@Target(ElementType.PARAMETER)
@interface ParamTest {
}

@Target(ElementType.METHOD)
@interface TestMethod {
}

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(TestRepeatedAnnotations.class)
@interface Words {
	String word() default "Test";

	int value() default 0;
}

@Retention(RetentionPolicy.RUNTIME)
@interface TestRepeatedAnnotations {
	Words[] value();
}

@Words(word = "The first")
@Words(word = "The second", value = 1)
public class SimpleTest {

	@SuppressWarnings({"checked", "deprecation"})
	public static void main(@ParamTest String[] args) throws ClassNotFoundException {

		@Words(word = "The third", value = 3)
		Subclass obj = new Subclass();

		BaseClass obj2 = new BaseClass();
		obj2.testMethod();

		obj.testMethod();

//		Class<?> c = SimpleTest.class;
//
//		System.out.println(c.getAnnotation(TestRepeatedAnnotations.class));

	}

}

class BaseClass {

	@Deprecated
	public void testMethod() {

	}

}

class Subclass extends BaseClass {

	@TestMethod
	@Override

	public void testMethod() {
		// TODO Auto-generated method stub
		super.testMethod();
	}

	@TestMethod
	public void test2Method() {

	}

}
=======
package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.Repeatable;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

import additional.TestImportInt;

@Target(ElementType.PARAMETER)
@interface ParamTest {
}

@Target(ElementType.METHOD)
@interface TestMethod {
}

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(TestRepeatedAnnotations.class)
@interface Words {
	String word() default "Test";

	int value() default 0;
}

@Retention(RetentionPolicy.RUNTIME)
@interface TestRepeatedAnnotations {
	Words[] value();
}

@Words(word = "The first")
@Words(word = "The second", value = 1)
public class SimpleTest  implements TestImportInt{
	
	private final String CONSTANT = "ConsT_1";
	
	@Override
	public String getConst() {
		// TODO Auto-generated method stub
		return CONSTANT;
	}
	
	@SuppressWarnings({"checked", "deprecation"})
	public static void main(@ParamTest String[] args) throws ClassNotFoundException {

		@Words(word = "The third", value = 3)
		Subclass obj = new Subclass();

		BaseClass obj2 = new BaseClass();
		obj2.testMethod();

		obj.testMethod();

//		Class<?> c = SimpleTest.class;
//
//		System.out.println(c.getAnnotation(TestRepeatedAnnotations.class));

	}

}

class BaseClass {

	@Deprecated
	public void testMethod() {

	}

}

class Subclass extends BaseClass {

	@TestMethod
	@Override

	public void testMethod() {
		// TODO Auto-generated method stub
		super.testMethod();
	}

	@TestMethod
	public void test2Method() {

	}

}
