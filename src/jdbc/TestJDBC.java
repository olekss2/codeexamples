package jdbc;

import java.io.BufferedReader;
import java.sql.*;
import java.io.InputStreamReader;

public class TestJDBC {

	public static void main(String[] args) {

		Connection con = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
			con.setAutoCommit(false);

			Connection con1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1/testJava", "root", "");
			con1.setAutoCommit(true);

//			Statement stmt = con.createStatement();
//
//			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

//			String id = reader.readLine();
//			String name = reader.readLine();
//			String surname = reader.readLine();
//			String city = reader.readLine();

//			Date date = new Date(878787);

//			stmt.execute(
//					"INSERT INTO `test_table`(`ID`, `NAME`, `SURNAME`, `CITY`,`DATE`) VALUES (2,\"J�nis)\",\"Kalni��\", \"R�ga\",\"20100403\")");

//			stmt.execute("INSERT INTO `test_table`(`ID`, `NAME`, `SURNAME`, `CITY`) VALUES (" + id + ",\"" + name
//					+ "\",\"" + surname + "\",\"" + city + "\")");
//			ResultSet rs = stmt.executeQuery("SELECT * FROM `test_table`");

//			int count = stmt.executeUpdate("DELETE FROM `test_table` WHERE `NAME` = \"" + name + "\";");
//

			String name = "P�teris";

			/// SELECT COLUMN1,COLUMN2,COLUMN3 .... WHERE ..... AND ... OR ....
			String sql = "update `test_table` set NAME=?, SURNAME=? where ID=?";
			PreparedStatement preparedStatement = con1.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setInt(3, 1);
			preparedStatement.setString(2, "Liepi��2");

			System.out.println(preparedStatement.executeUpdate());
//			con1.rollback();
//			con1.close();
//
//			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/testJava", "root", "");
//			stmt = con.createStatement();
//			ResultSet rs = stmt.executeQuery("SELECT * FROM `test_table`");
//			while (rs.next())
//				System.out.println(rs.getString("ID") + "; " + rs.getString("NAME") + "; " + rs.getString("SURNAME")
//						+ "; " + rs.getString("CITY") + " " + rs.getDate("DATE"));

//			System.out.println("Count is:" + count);

		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				con.close();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

}
