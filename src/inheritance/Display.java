package inheritance;

public class Display {

	private static void processAnimal(Animal animal) {

		try {

			Fish fish = (Fish) animal;

		} catch (ClassCastException e) {
			System.out.println("Exception is caught, I'm is not the fish!");

			Bird bird = (Bird) animal;
			System.out.println("I'm is not the bird!");
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Amphibia amphibia = new Amphibia();
		amphibia.initilize();
		amphibia.move("ground");

//		Object instance = Hidden.getAnimal();
//
////		if (instance instanceof Bird)
////			System.out.println("I'm the bird!");
////		else if (instance instanceof Fish)
////			System.out.println("I'm the fish!");
//
//		System.out.println(instance.getClass());
//
//		Bird instanceOfAnimal = (Bird) Hidden.getAnimal();
//		Display.processAnimal(instanceOfAnimal);

//		Animal instanceOfAnimal2 = new Animal();

//		instanceOfAnimal = (Bird) instanceOfAnimal2;

//

//		Object object = instanceOfAnimal;
//
//		System.out.println(object instanceof Animal);
//
//		Animal animal = (Animal) object;

//		try {
//			Bird bird = (Bird) instanceOfAnimal2;
//		} catch (ClassCastException e) {
//			// TODO: handle exception
//			System.out.println("Exception is cought!");
//		}

//		instanceOfAnimal.setName("Jack");
//		instanceOfAnimal.setAge(3);
//		instanceOfAnimal.getDetails();
//
//		Bird instanceOfBird = (Bird) instanceOfAnimal;
//		System.out.println(instanceOfBird.getWings());
//
//		Hidden.justImport((Bird) instanceOfAnimal);

	}

}

class Animal {
	protected String name;
	protected int age;

	protected Animal() {

	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return this.age;
	}

	public void getDetails() {
		System.out.println("My name is " + this.name + ", I'm " + this.age + " years old");
	}

	protected void move() {
		// To be implemented in the sub-classes
	}

}

class Bird extends Animal {

	protected int numberOfWings = 2;

	public void setWings(int numberOfWings) {
		this.numberOfWings = numberOfWings;
	}

	public int getWings() {
		return this.numberOfWings;
	}

	@Override
	public void getDetails() {
		super.getDetails();
		System.out.println("I'm the bird, My name is " + this.name + ", I'm " + this.age + " years old" + ", I have "
				+ this.numberOfWings + " wings");
	}

}

class Fish extends Animal {

	@Override
	public void getDetails() {
		super.getDetails();
		System.out
				.println("I'm the fish, my name is " + this.name + ", I'm " + this.age + " years old" + ", I can swim");
	}

	public void swim() {
		System.out.println("I'm swimming!");
	}

	@Override
	public void move() {
		this.swim();
	}
}

class Human extends Animal {

	@Override
	public void move() {
		System.out.println("I'm walking");
	}

}

class Amphibia {
	private Fish shark;
	private Human human;

	public void initilize() {
		this.shark = new Fish();
		this.human = new Human();
	}

	public void move(String media) {
		if (media == "water")
			this.shark.move();
		if (media == "ground")
			this.human.move();
	}

}

class Hidden {

	public static Animal getAnimal() {
		return new Bird();
	}

	public static void justImport(Bird bird) {

	}

}