package inheritance;

import oop.*;

public class Employee2 {
	Account account;
	Square square;
	
	private static final double BASE_SALARY = 15000.00;
	private String name;
	private double salary;
	private Date birthDate;

	public Employee2(String name, double salary, Date DoB) {
		this.name = name;
		this.salary = salary;
		this.birthDate = DoB;
	}

	public Employee2(String name, double salary) {
		this(name, salary, null);
	}

	public Employee2(String name, Date DoB) {
		this(name, BASE_SALARY, DoB);
	}

	public Employee2(String name) {
		this(name, BASE_SALARY);
	}// more Employee code... }

	public static void main(String[] args) {
		Date date = new Date();
		date.setDay(4);
		date.setMonth(12);
		date.setYear(1990);
		Employee2 employee = new Employee2("Janis", 30000, date);
	}

	public String getDoB() {
		return this.birthDate.getDay() + "." + this.birthDate.getMonth() + "." + this.birthDate.getYear();
	}

}

class Manager2 extends Employee2 {
	private String department;

	public Manager2(String name, double salary, String dept) {
		super(name, salary);
		department = dept;
	}

	public Manager2(String name, String dept) {
		super(name);
		department = dept;
	}
}

class Manager3 {

	private Employee2 employee;
	private String department;

	public Manager3(String name, double salary, Date DoB) {
		employee = new Employee2(name, salary, DoB);
	}

}
