package inheritance;

class SuperSuperEx{

	protected SuperSuperEx() {
		// TODO Auto-generated constructor stub
		System.out.println("Super Super class is called");
	}
}

class SuperClassEx extends SuperSuperEx {

	protected SuperClassEx(int i) {
		// TODO Auto-generated constructor stub
		System.out.println("Super class is called");
	}

}

public final class ContructorsExample extends SuperClassEx {

	protected ContructorsExample(int i) {
		super(i);
		System.out.println("Sub class is called");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ContructorsExample instance = new ContructorsExample();
	}

}

class SubContrstructors extends ContructorsExample {

}
