package oop;

public class TightCoupling {
	
	public static void main(String[] args) {

		B ob = new B();
		ob.name = null;
		System.out.println("Name is " + ob.name);
	}
	
}

class B {
	public String name;

	public String getName() {
		if (this.name != null) {
			return this.name;
		}
		return "not initialized";

	}

	public void setName(String s) {
		if (s == null) {
			System.out.println("You can't initialize the name to a null");
		} else {
			this.name = s;
		}
	}
	
}
