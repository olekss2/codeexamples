package oop;

public class StructuredCode {

	static int account_number = 20;
	static int account_balance = 100;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		StructuredCode.account_balance = StructuredCode.account_balance + 100;
		StructuredCode.showData();
		
		StructuredCode.account_balance = StructuredCode.account_balance - 50;
		StructuredCode.showData();
		
		StructuredCode.account_balance = StructuredCode.account_balance - 10;
		StructuredCode.showData();
		
	}

	private static void showData() {
		System.out.println("Account Number = " + StructuredCode.account_number);
		System.out.println("Account Balance = " + StructuredCode.account_balance);
	}
}
