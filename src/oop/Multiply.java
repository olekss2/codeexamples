package oop;

///Not high cohesive behavior
public class Multiply {

	private int a = 3;
	private int b = 4;

	public int mul(int a, int b) {
		this.a = a;
		this.b = b;
		return a * b;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Multiply m = new Multiply();
		System.out.println(m.mul(2, 7));
	}

}
