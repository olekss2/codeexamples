package oop;

public class TestPassByValRef {

	int a = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestPassByValRef instance = new TestPassByValRef();
		TestPassByValRef.changeRef(instance);
		System.out.println(instance.a);
	}

	private static void changeRef(TestPassByValRef instance) {
		instance = new TestPassByValRef();
		instance.a = 12;
	}

}
