package oop;

//High cohesive example

public class DisplayPerson extends Object{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name = args[0];
		int age = Integer.parseInt(args[1]);
		int number = Integer.parseInt(args[2]);
		Person person = new Person(name, age, number);
		System.out.println("Person's name is: " + person.getName() + ", person is " + person.getAge() + " years old, "
				+ "mobile number: " + person.getMobileNumber());

	}

}

class Person {

	private Name name = new Name();
	private Age age = new Age();
	private Number number = new Number();

	public Person(String name, int age, int number) {
		// TODO Auto-generated constructor stub
		this.name.setName(name);
		this.age.setAge(age);
		this.number.setMobileNumber(number);
	}

	public String getName() {
		return this.name.getName();
	}

	public int getAge() {
		return this.age.getAge();
	}

	public int getMobileNumber() {
		return this.number.getMobileNumber();
	}

}

class Name {
	private String name;

	public String getName() {
		if (!(this.name == null))
			return this.name;
		else
			return "Name is not set!";
	}

	public void setName(String name) {
		if (!(name == null)) {
			this.name = name;
		}
	}

}

class Age {
	private int age;

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}

class Number {
	private int mobilenumber;

	public int getMobileNumber() {
		return this.mobilenumber;
	}

	public void setMobileNumber(int mobilenumber) {
		this.mobilenumber = mobilenumber;
	}

}
