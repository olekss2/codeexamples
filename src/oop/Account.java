package oop;

public class Account {

	private int account_number;
	private int account_balance;

	public void showData() {
		System.out.println("Account Number = " + this.account_number);
		System.out.println("Account Balance = " + this.account_balance);
	}

	public void deposit(int a, String username) {
		if (a < 0) {
			System.out.println("Error");
		} else if (username == "M�ris") {
			System.out.println("M�ris cannot change the account balance. He is blocked!");
		} else {
			this.account_balance += a;
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
