package oop;

public class Display {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Multiplication m = new Multiplication();
		System.out.println(m.mul(2, 7));
	}
}

class Multiplication {

	private int a = 3;
	private int b = 4;
	private int c = 1;

	public int mul3(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
		return a * b * c;
	}

	public int mul(int a, int b) {
		this.a = a;
		this.b = b;
		return a * b;
	}

}
