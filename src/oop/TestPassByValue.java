package oop;

public class TestPassByValue {

	private int a = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestPassByValue instance = new TestPassByValue();
		instance.a = 2;

		instance.passParameters(instance);
		System.out.println(instance.a);
	}

	private void passParameters(TestPassByValue instance2) {
		TestPassByValue instance3 = new TestPassByValue();
		instance3.a = 1;
		instance2 = instance3;
	}

}
