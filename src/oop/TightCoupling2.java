package oop;

public class TightCoupling2 {

	Topic t = new Topic();

	public void startReading() {
		this.t.understand();
	}

}

class Topic {
	public void understand() {
		System.out.println("Tight coupling concept");
	}
}
