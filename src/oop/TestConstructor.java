package oop;

public class TestConstructor {

	private int a = 0;

	static {
		System.out.println("Static constructor!");
	}

	private TestConstructor() {
		// TODO Auto-generated constructor stub
		System.out.println("Constructor with no parameters called!");
	}

	private TestConstructor(int a) {
		// TODO Auto-generated constructor stub
		System.out.println("Constructor with 1 parameter called!");
		this.a = a;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		TestConstructor instance = new TestConstructor();
//		TestConstructor instance2 = new TestConstructor(3);

	}

	public static TestConstructor getInstance() {
		return new TestConstructor();
	}

}

class TestConstructor2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TestConstructor instance = TestConstructor.getInstance();

	}

}
