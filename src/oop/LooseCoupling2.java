package oop;

public class LooseCoupling2 {

	public static void main(String[] args) {

		TopicInterface t = new Topic2();
		t.understand();
	}
}

class Topic2 implements TopicInterface {

	public void understand() {
		System.out.println("Got it");
	}

}

class Topic3 implements TopicInterface {

	public void understand() {
		System.out.println("understand");
	}

}

interface TopicInterface {

	void understand();

}
