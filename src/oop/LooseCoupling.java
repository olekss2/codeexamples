package oop;

public class LooseCoupling {
	
	public static void main(String[] args) {

		C ob = new C();
		ob.setName(null);
		System.out.println("Name is " + ob.getName());
	}
	
}

class C {
	private String name;

	public String getName() {
		if (this.name != null) {
			return this.name;
		}
		return "not initialized";

	}

	public void setName(String s) {
		if (s == null) {
			System.out.println("You can't initialize the name to a null");
		} else {
			this.name = s;
		}
	}
	
}
