package oop;

public class Rectangle extends Square {

	protected int width = 0;

	public Rectangle(int height, int width) {
		super(height);
		this.width = width;
	}

	@Override
	public int getArea() {
		return this.height * this.width;
	}

}
