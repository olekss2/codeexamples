package HWks;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {
	private byte sails;
	private int wheels ;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;
		this.sails = sails;
	}

	@Override
	public String move(Road road) {
		String status = "";
		if (road instanceof WaterRoad) {
			WaterRoad waterRoad = (WaterRoad) road;
			return getType() + " is sailing on " + waterRoad + " with " + sails + " sails";
		}

		if (road instanceof Road) {
			if (road.getClass() == Road.class) {
				
				status = super.move(road);
				if (!status.startsWith("Cannot"))
					return getType() + " is driving on " + road + " with " + wheels + " wheels";
				else
					return status;
			}
			return "Cannot drive on " + road;
		}
		return status;

	}

}
