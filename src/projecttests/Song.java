package projecttests;

public class Song {
    private int ID;
    private String song;
    private String artist;
    private String genre;
    private int  releaseYear;




    public int  getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    public String getSong() {
        return song;
    }

    public void setName(String song) {
        this.song = song;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int  getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }


}