package projecttests;

import java.util.List;

public abstract class FileType {
	protected List<Song> manysongs; // store the data we recieved from file; it is the common object
	abstract void populateData(String content) throws Exception;

	public List<Song> getSong() {
		return this.manysongs;
	}

	public static FileType getInstance(String fileType) {
		switch (fileType) {
		case "xml":
			return new XML();
		default:
			return null;
		}
	}
}