package projecttests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Migration {

	private BufferedReader reader;
	private String content;

	public void setFile(String filepath) throws FileNotFoundException, IOException {
		File file = new File(filepath);
		reader = new BufferedReader(new java.io.FileReader(file));
	}

	public String getContent() throws IOException {
		if (!(this.content == null))
			return this.content;

		StringBuffer content = new StringBuffer();
		String line = this.reader.readLine();
		while (line != null) {
			content.append(line);
			content.append(System.lineSeparator());
			line = this.reader.readLine();
		}

		this.content = content.toString();
		return this.content;
	}

	public static void main(String[] args) throws Exception{

		System.out.println("Enter the filepath:");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String path = reader.readLine();
		
		Migration migrate = new Migration();
		migrate.setFile(path);
		
		
		FileType fileType = FileType.getInstance("xml");
		fileType.populateData(migrate.getContent());

	}

}
