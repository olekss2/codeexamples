package projecttests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.List;

public class Database {

	private Connection conn = null;

	public Database() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
		this.conn.setAutoCommit(false);
	}

	public void setEntries(List<Song> manysongs) throws Exception {

		String sqlInsert = "INSERT INTO songs.songs3 ( ID, Name, Artist, Genre, ReleaseYear) values (?,?,?,?,?)";
		PreparedStatement statement = this.conn.prepareStatement(sqlInsert);

		Iterator<Song> iterator = manysongs.iterator();

		while (iterator.hasNext()) {
			Song currentSonggetters = iterator.next();
			statement.setInt(1, currentSonggetters.getID());
			statement.setString(2, currentSonggetters.getSong());
			statement.setString(3, currentSonggetters.getArtist());
			statement.setString(4, currentSonggetters.getGenre());
			statement.setInt(5, currentSonggetters.getReleaseYear());

			statement.addBatch();
		
		int[] count = statement.executeBatch();
		statement.executeBatch();
		boolean success = false;
		for (int countItem : count) {
			if (countItem == 0)
				System.out.println("Some entries are not updated");
			else 
               success = true;
		}
		
		if(success) {
			this.conn.commit();
			System.out.println("Entries are updated");
		}
		}
	}
}
