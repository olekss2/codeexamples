package javaSyntax2;

import javaSyntax.AccessModifiers;

public class AccessModifiers2 {

	private AccessModifiers l_instance = new AccessModifiers();

	private void Test() {

		this.l_instance.a = 1;// Default
		this.l_instance.b = 1;// Private
		this.l_instance.c = 1;// Protected
		this.l_instance.d = 1;// Public

	}

}
