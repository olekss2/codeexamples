package javaSyntax2;

import javaSyntax.AccessModifiers;
import oop.Contructors;

public class AccessModifers2Sub extends AccessModifiers {
	
	private void Test() {

		this.a = 1;// Default
		this.b = 1;// Private
		this.c = 1;// Protected
		this.d = 1;// Public

	}
}
