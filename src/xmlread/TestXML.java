package xmlread;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TestXML {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			String path = "./src/xmlread/Example.xml";

			Document document = TestXML.getDocument(path);
			TestXML.processDoc(document);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private static Document getDocument(String path) throws Exception {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		return docBuilder.parse(new File(path));

	}

	private static void processDoc(Document document) {
		Element breakfastMenuElement = document.getDocumentElement();

		FoodMenu breakfastMenu = new FoodMenu();
		List<Food> foodList = new ArrayList<Food>();
		breakfastMenu.setFoods(foodList);

		NodeList foodElements = breakfastMenuElement.getElementsByTagName("food");

		for (int i = 0; i < foodElements.getLength(); i++) {
			Element foodNode = (Element) foodElements.item(i);
			Food food = new Food();

			NodeList foodProperties = foodNode.getChildNodes();

			for (int j = 0; j < foodProperties.getLength(); j++) {
				Node parameter = foodProperties.item(j);

				switch (parameter.getNodeName()) {
				case "name":
					food.setName(parameter.getTextContent());
					break;
				case "price":
					food.setPrice(Float.parseFloat(parameter.getTextContent().replaceAll("[^0-9.]", "")));
					break;
				case "calories":
					food.setCalories(Integer.parseInt(parameter.getTextContent()));
					break;
				case "description":
					food.setDescription(parameter.getTextContent());
				default:
					break;
				}

			}
			foodList.add(food);

		}
		System.out.println("End of programm");
	}

}
