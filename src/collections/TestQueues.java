package collections;

import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class TestQueues {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<String> queue = new LinkedList<String>();
		queue.offer("Janis");
		queue.offer("Peteris");
		queue.offer("Davis");
		System.out.println(queue.poll());

	}

}
