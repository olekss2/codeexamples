package collections;

import java.util.ListIterator;

public class TestLinkedList {

	public static void main(String[] args) {

		java.util.LinkedList<String> list = new java.util.LinkedList<String>();

		list.add("Element1");
		list.add("Element2");
		list.add("Element2");
		list.add("Element3");
		list.add("Element2");
//		System.out.println(list);
//		System.out.println(list.lastIndexOf("Element2"));

		ListIterator<String> iter = list.listIterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

	}

}
