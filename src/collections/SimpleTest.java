package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

public class SimpleTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Set<String> set = new TreeSet<String>();

		set.add("Name3");
		set.add("Name2");
		set.add("Name5");
		set.add("Name1");
		set.add("Name5");
		set.add("Name7");
		set.add("Name8");
		System.out.println(set);

//
//		List<Integer> list = new Vector<Integer>();
//		list.add(4324);
//
//		Set<Integer> set = new TreeSet<Integer>();
//		set.add(43);
//		set.add(12);
//		set.add(43);
//		set.add(1);
//
//		PriorityQueue<String> queue = new PriorityQueue<String>();
//
//		queue.add("Janis");
//		queue.add("Peteris");
//		queue.add("Davis");
//		queue.poll();
//		queue.remove();
//
//		Map<Integer, String> map = new TreeMap<Integer, String>();
//
//		map.put(1121, "Janis");
//		map.put(1212, "Peteris");
//		map.put(1121, "Davis");
//		map.put(2161, "Davis");
//
//		Map<Integer, String> map2 = new TreeMap<Integer, String>();
//		map2.put(1121, "Jekabs");
//		map2.putAll(map);
//
////		System.out.println(map2.toString());
//
//		Map<Integer, TableClassTest> mapTable = new TreeMap<Integer, TableClassTest>();
//
//		mapTable.put(1, new TableClassTest(723, "I'm the value", true));
//		mapTable.put(2, new TableClassTest(321433, "I'm the second value", false));
//		System.out.println(mapTable.toString());
//		
//		Map<CustomKey, TableClassTest> mapTable2 = new TreeMap<CustomKey, TableClassTest>();
//
//		CustomKey keyObj1 = new CustomKey();
//		keyObj1.key1 = "KeyString1";
//		keyObj1.key2 = 423432;
//
//		CustomKey keyObj2 = new CustomKey();
//		keyObj2.key1 = "KeyString1";
//		keyObj2.key2 = 423432;

//		mapTable2.put(keyObj1, new TableClassTest(723, "I'm the value", true));
//		mapTable2.put(keyObj2, new TableClassTest(321433, "I'm the second value", false));

//		System.out.println(mapTable2.toString());
//		Iterator iterator = queue.iterator();
//
//		while (iterator.hasNext())
//			System.out.println(iterator.next());

//		System.out.println(set.toString());

//		list.add("Text");
//		list.add(new SimpleTest());

	}

}

class TableClassTest {

	public int value1;
	public String value2;
	public boolean value3;

	public TableClassTest(int value1, String value2, boolean value3) {
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
	}

	@Override
	public String toString() {
		return "value1 = " + this.value1 + ", value2 = " + this.value2 + ", value3 = " + this.value3;
	}

}

class CustomKey {

	public String key1;
	public int key2;

	@Override
	public String toString() {
		return "key1 = " + this.key1 + ", key2 = " + this.key2;
	}

}
=======
package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import additional.TestImportInt;

public class SimpleTest implements TestImportInt {

	public final String CONSTANT2 = "SOMETHING";

	@Override
	public String getConst() {
		// TODO Auto-generated method stub
		return CONSTANT2;
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		try {

			List<String> list1 = new ArrayList<>();
			List<String> list2 = new ArrayList<>();

			list1.add("Element1");
			list1.add("Element2");
			list1.add("Element3");

			list2.add("Element3");
			list2.add("Element2");
//			list2.add("Element6");

//			System.out.println(list1.containsAll(list2));
			ListIterator<String> listIt = list1.listIterator();

			while (listIt.hasNext()) {
				String element = listIt.next();
				listIt.remove();
				listIt.add(element + "++");
			}

			System.out.println(list1);
//			System.out.println(listIt.previous());
//			System.out.println(listIt.previous());

			List listNoType = new ArrayList<>();

			listNoType.add("Element1");
			String element = (String) listNoType.get(0);

			List<Integer> list = new Vector<Integer>();
			list.add(4324);

			Set<Integer> set = new TreeSet<Integer>();
			set.add(43);
			set.add(12);
			set.add(43);
			set.add(1);

			PriorityQueue<String> queue = new PriorityQueue<String>();

			queue.add("Janis");
			queue.add("Peteris");
			queue.add("Davis");
			queue.poll();
			queue.remove();

			Map<Integer, String> map = new TreeMap<Integer, String>();

			map.put(1121, "Janis");
			map.put(1212, "Peteris");
			map.put(1121, "Davis");
			map.put(2161, "Davis");

			Map<Integer, String> map2 = new TreeMap<Integer, String>();
			map2.put(1121, "Jekabs");
			map2.putAll(map);

//		System.out.println(map2.toString());

			Map<Integer, TableClassTest> mapTable = new TreeMap<Integer, TableClassTest>();

			mapTable.put(1, new TableClassTest(723, "I'm the value", true));
			mapTable.put(2, new TableClassTest(321433, "I'm the second value", false));
//		System.out.println(mapTable.toString());

			Map<CustomKey, TableClassTest> mapTable2 = new TreeMap<CustomKey, TableClassTest>();

			CustomKey keyObj1 = new CustomKey();
			keyObj1.key1 = "KeyString1";
			keyObj1.key2 = 423432;

			CustomKey keyObj2 = new CustomKey();
			keyObj2.key1 = "KeyString1";
			keyObj2.key2 = 423431;

			mapTable2.put(keyObj1, new TableClassTest(723, "I'm the value", true));
			mapTable2.put(keyObj2, new TableClassTest(321433, "I'm the second value", false));

//			System.out.println(mapTable2.toString());
//			System.out.println(mapTable2.values().toString());
//		Iterator iterator = queue.iterator();
//
//		while (iterator.hasNext())
//			System.out.println(iterator.next());

//		System.out.println(set.toString());

//		list.add("Text");
//		list.add(new SimpleTest());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

class TableClassTest {

	public int value1;
	public String value2;
	public boolean value3;

	public TableClassTest(int value1, String value2, boolean value3) {
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
	}

	@Override
	public String toString() {
		return "value1 = " + this.value1 + ", value2 = " + this.value2 + ", value3 = " + this.value3;
	}

}

class CustomKey implements Comparable<CustomKey> {

	/*
	 * CustomKey obj1 = new..; CustomKey obj2 = new..;
	 * 
	 * obj1.compareTo(obj2); => obj1 = obj2
	 */

	public String key1;
	public int key2;

	public int compareTo(CustomKey o) {

//		return 0;

		return this.getKey().compareTo(o.getKey());
	};

	@Override
	public String toString() {
		return "key1 = " + this.key1 + "; key2 = " + this.key2;
	}

	public String getKey() {
		return this.key1 + this.key2;
	}
}

class Employee implements Comparable<Employee> {

	public int skillLevel;
	public int age;

	@Override
	public int compareTo(Employee o) {

		int total1 = this.skillLevel * 50 - this.age * 10;
		int total2 = o.skillLevel * 50 - this.age * 10;

		if (total1 > total2)
			return 1;
		else if (total1 < total2)
			return -1;
		else
			return 0;
	}

}
>>>>>>> branch 'master' of https://olekss2@bitbucket.org/olekss2/codeexamples.git
