package abstraction;

interface Printable {

	void print();

}

interface Showable {
	String state = "Is showing";

	void show();

}

class C implements Printable {
	@Override
	public void print() {
		// TODO Auto-generated method stub

	}
}

public class TestInterfaces implements Printable, Showable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Printable instance = new TestInterfaces();
		instance.print();

	}

	@Override
	public void print() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

}
