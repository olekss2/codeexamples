package abstraction;

public class TestVirtuals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Computer computer = UtilClassTest.getComputer();
		computer.powerOn();

	}

}

interface Computer {

	void powerOn();

}

interface Phone extends Computer {

	void call();

}

abstract class Samsung implements Phone {

	@Override
	public void powerOn() {
		// TODO Auto-generated method stub

	}

	@Override
	public void call() {
		// TODO Auto-generated method stub

	}

}

class IPhone implements Phone {
	@Override
	public void call() {
		// TODO Auto-generated method stub

	}

	@Override
	public void powerOn() {
		// TODO Auto-generated method stub

	}
}

class MacBook implements Computer {

	String state = "Off";

	@Override
	public void powerOn() {
		// TODO Auto-generated method stub
		this.state = "On";
	}

}

abstract class UtilClassTest {

	public static Computer getComputer() {
		return new SurfacePro();
	}

}
