package abstraction;

public class TestLocalInners {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LocalInner obj = new LocalInner();
		obj.display();
		
		LocalInner2 obj2 = new LocalInner2();
		obj2.display();

	}

}

class LocalInner {
	private int data = 20;

	public void display() {

		class Local {
			void msg() {
				System.out.println(LocalInner.this.data);
			}
		}

		Local lobjc = new Local();
		lobjc.msg();

	}

}

class LocalInner2{
	private int data= 40;
	
	public void display() {
		int lvalue = 43;
		
		class Local{
			void msg() {
				System.out.println(lvalue);
			}
		}
		
		Local lobj = new Local();
		lobj.msg();
		
	}
}
