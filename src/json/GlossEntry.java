package json;

public class GlossEntry {
	private String ID;
	private String sortAs;
	private String glossTerm;
	private String Acronym;
	private String Abbrev;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getSortAs() {
		return sortAs;
	}
	public void setSortAs(String sortAs) {
		this.sortAs = sortAs;
	}
	public String getGlossTerm() {
		return glossTerm;
	}
	public void setGlossTerm(String glossTerm) {
		this.glossTerm = glossTerm;
	}
	public String getAcronym() {
		return Acronym;
	}
	public void setAcronym(String acronym) {
		Acronym = acronym;
	}
	public String getAbbrev() {
		return Abbrev;
	}
	public void setAbbrev(String abbrev) {
		Abbrev = abbrev;
	}
}
