package json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {
	public static void main(String[] args) throws IOException {

//		File myObj = new File("./src/json/Glossary_store.txt");
		String path = "./src/json/Glossary_store.txt";
		String content;
		try (Stream<String> lines = Files.lines(Paths.get(path))) {

			// default StandardCharsets.UTF_8
			content = lines.collect(Collectors.joining(System.lineSeparator()));
//			System.out.println(content);

			JSONObject jsonObject = new JSONObject(content);

			Glossary glossary = new Glossary();

			JSONObject glossaryJson = jsonObject.getJSONObject("glossary");
			String title = glossaryJson.getString("title");
			glossary.setTitle(title);

			JSONObject glossDiv = glossaryJson.getJSONObject("GlossDiv");
			char division = glossDiv.getString("title").charAt(0);
			glossary.setDivision(division);

			JSONObject glossList = glossDiv.getJSONObject("GlossList");
			JSONArray glossItems = glossList.getJSONArray("GlossEntries");

			List<GlossEntry> glosseries = new ArrayList<GlossEntry>();
			glossary.setGlosseries(glosseries);

			Iterator<Object> iterator = glossItems.iterator();

			while (iterator.hasNext()) {
				JSONObject item = (JSONObject) iterator.next();
				GlossEntry glossItem = new GlossEntry();

				glossItem.setID(item.getString("ID"));
				glossItem.setSortAs(item.getString("SortAs"));
				glossItem.setGlossTerm(item.getString("GlossTerm"));
				glossItem.setAcronym(item.getString("Acronym"));
				glossItem.setAbbrev(item.getString("Abbrev"));
				glosseries.add(glossItem);
			}

			System.out.println("Over");
//			JSONArray jsonGrossery = new JSONArray(content); // Error [
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		System.out.println("Over");

//		try {
//			JSONParser parser = new JSONParser();
//			JSONArray array = (JSONArray) parser.parse(content);
//		} catch (ParseException pe) {
//
//			System.out.println("position: " + pe.getPosition());
//			System.out.println(pe);
//		}

	}

}
