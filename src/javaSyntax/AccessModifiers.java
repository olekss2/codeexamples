package javaSyntax;

public class AccessModifiers {

	int a = 0;// Default
	private int b = 0;// Private
	protected int c = 0;// Protected
	public int d = 0;// Public

	private void Test() {
		this.a = 1;
		this.b = 1;
		this.c = 1;
		this.d = 1;
	}

}

class AccessModifiers2 {

	private AccessModifiers l_instance = new AccessModifiers();

	private void Test() {

		this.l_instance.a = 1;// Default
		this.l_instance.b = 1;// Private
		this.l_instance.c = 1;// Protected
		this.l_instance.d = 1;// Public

	}

}
