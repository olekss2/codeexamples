package javaSyntax;

public class Comments {

	/*
	 * Multi-line comments � or block of comments
	 */

	/**
	 * Documentation comments � block of comment which is used to comment JavaDoc
	 * This documentation comment!
	 */

	/*- Unformatted block comment*/

	void mainvoid() {
		int a = 1;
		int b = 2;

		/*
		 * a = a + b + 1; // Simple comment a = a + 2;
		 */

		/*
		 * a = a + b + 1; Here is the comment!
		 */

	}

}
