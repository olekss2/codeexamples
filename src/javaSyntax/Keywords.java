package javaSyntax;

public class Keywords {

	/// Enum
	enum Level {
		LOW, MEDIUM, HIGH
	};

	Level MyKnowledge = Level.MEDIUM;

	/// goto

//	private void Test() {
//      goto;
//	}

	public static void main(String[] args) {

//		try {
//			String inputString = args[0];
//		}
//
//		catch (ArrayIndexOutOfBoundsException ex) {
//			System.out.println("Array index out of bounds!");
//		}
//
//		finally {
//			System.out.println("End of program");
//		}

//		int a = 0;
//		System.out.println(a++);
//		System.out.println(a);
		
	}
}
