package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;

public class FirstExample {

	public static void main(String[] args) throws Exception {

		File file = new File("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\PrintedFIle.csv");
		try {

			BufferedReader reader = new BufferedReader(new FileReader(file));
			String s;
			s = reader.readLine();
			while (s != null) {
				System.out.println(s);
				s = reader.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

//		FileOutputStream file = new FileOutputStream(
//				"C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\PrintedFIle.txt");
//
//		PrintStream printStObj = new PrintStream(file);
//		printStObj.println("The first line1");
//		printStObj.println("The second line2");
//		printStObj.close();
//		file.close();
//		System.out.println("File is closed");

		//// row1, row2, row3,...

//		System.out.println("Output");
//		System.err.println("Error");
//		throw new Exception("Error");

//		int letter[] = new int[5];
//
//		for (int i = 0; i < 5; i++) {
//			letter[i] = System.in.read();
//			System.out.println(letter[i]);
//		}
//		
//		System.out.println(letter.toString());

//		FileOutputStream file = new FileOutputStream("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\File.txt");
//		String text = "Text is here, using the Bytes";
//		byte[] textBytes = text.getBytes();
//
//		for (int i = 0; i < textBytes.length; i++)
//			System.out.println(textBytes[i] + " ");

//		for (int i = 0; i < text.length(); i++) {
//			file.write(text.charAt(i));
//		}
//		
//		file.write(text.getBytes());
//		file.close();
//		System.out.println("Succesfully written");

//		FileInputStream file = new FileInputStream("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\File.txt");
//		int i = 0;
//
//		while ((i = file.read()) != -1)
//			System.out.print((char) i);
//		file.close();

//		Reader reader = new InputStreamReader(
//				new FileInputStream("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\File.txt"), "UTF-8");
//
//		int i = 0;
//
//		while ((i = reader.read()) != -1)
//			System.out.print((char) i);
//		reader.close();

	}

}
