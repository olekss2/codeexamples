package xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TestXMLExample {

	public static void main(String args[]) {
		try {

			TestXMLExample instance = new TestXMLExample();
			Document document = instance.getDocument("./src/xml/Example.xml");
			instance.processDoc(document);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private Document getDocument(String path) throws Exception {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		return docBuilder.parse(new File(path));

	}

	private void processDoc(Document document) {
		Element breakfastMenuElement = document.getDocumentElement();
		BreakFastMenu breakfastMenu = new BreakFastMenu();
		List<Food> foodList = new ArrayList<Food>();
		breakfastMenu.setFoods(foodList);

		NodeList foodElements = breakfastMenuElement.getElementsByTagName("food");

		for (int i = 0; i < foodElements.getLength(); i++) {
			Node foodNode = foodElements.item(i);
			System.out.println(foodNode.getNodeName());
			Food food = new Food();

			NodeList foodProperties = foodNode.getChildNodes();

			for (int j = 0; j < foodProperties.getLength(); j++) {
				Node parameter = foodProperties.item(j);
				switch (parameter.getNodeName()) {
				case "name":
					food.setName(parameter.getTextContent());
					break;
				case "price":
					System.out.println(parameter.getTextContent().replaceAll("[^0-9.]", ""));
					food.setPrice(Float.parseFloat(parameter.getTextContent().replaceAll("[^0-9.]", "")));
					break;
				case "calories":
					food.setCalories(Integer.parseInt(parameter.getTextContent()));
					break;
				case "description":
					food.setDescription(parameter.getTextContent());
					break;
				default:
					break;
				}

			}
			foodList.add(food);

		}

		System.out.println("Done");
	}

}
