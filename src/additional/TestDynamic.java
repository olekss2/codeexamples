package additional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestDynamic {

	public static void main(String[] args) {

		Object instance = new BD();

		try {
			System.out.println(instance.getClass().getSimpleName());
			Method method = instance.getClass().getMethod("test", null);
			method.invoke(instance, null);
		} catch (NoSuchMethodException | InvocationTargetException
				| IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}