package additional;

import java.util.LinkedList;

public class Virus implements Cloneable {

	private LinkedList<String> historicalOwners = new LinkedList<String>();

	public static void main(String[] args) throws Exception {

		Person person1 = new Person("J�nis");
		person1.setVirus(new Virus());

		Person person2 = new Person("P�teris");
		person1.meet(person2);

		Person person3 = new Person("M�ris");
		person2.meet(person3);

		Person person4 = new Person("M�rti��");
		person3.meet(person4);

		Person person5 = new Person("Reinis");
		person4.meet(person5);

		System.out.println(person1.getName() + " has the virus with the historical holders - "
				+ person1.getVirus().getHistoricalOwners());
		System.out.println(person2.getName() + " has the virus with the historical holders - "
				+ person2.getVirus().getHistoricalOwners());
		System.out.println(person3.getName() + " has the virus with the historical holders - "
				+ person3.getVirus().getHistoricalOwners());
		System.out.println(person4.getName() + " has the virus with the historical holders - "
				+ person4.getVirus().getHistoricalOwners());
		System.out.println(person5.getName() + " has the virus with the historical holders - "
				+ person5.getVirus().getHistoricalOwners());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Virus virus = new Virus();
		virus.setHistoricalOwners((LinkedList<String>) this.historicalOwners.clone());
		return virus;
	}

	public void setHistoricalOwners(LinkedList<String> historicalOwners) {
		this.historicalOwners = historicalOwners;
	}

	public LinkedList<String> getHistoricalOwners() {
		return historicalOwners;
	}

}

class Person {

	private String name;
	private int antibodies = 0;
	private Virus virus = null;
	private boolean wearsMask = false;

	public Person(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAntibodies(int antibodies) {
		this.antibodies = antibodies;
	}

	public void setVirus(Virus virus) {
		this.virus = virus;
	}

	public int getAntibodies() {
		return antibodies;
	}

	private void shareVirus(Person person) throws Exception {
		if (person.getAntibodies() == 0 && !this.wearsMask) {
			person.setVirus((Virus) this.virus.clone());
			person.getVirus().getHistoricalOwners().add(this.name);
		}

	}

	public Virus getVirus() {
		return virus;
	}

	public void meet(Person person) throws Exception {

		if (this.virus != null)
			this.shareVirus(person);
		else if (person.getVirus() != null)
			person.shareVirus(this);
	}

}