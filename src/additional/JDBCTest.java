package additional;

import java.sql.*;

public class JDBCTest {

	public static void main(String[] args) throws Exception {

		Connection con = null;
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");

//			con = DriverManager.getConnection("jdbc:mysql://localhost:8080/mysql", "root", "");
//			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/mysql/?autoReconnect=true&useSSL=false",
			con = DriverManager.getConnection("jdbc:mysql://192.168.64.2/mysql", "root", "");
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM `test_table`");

			while (rs.next())
				System.out.println(rs.getInt("ID") + "; " + rs.getString("NAME") + "; " + rs.getString("SURNAME") + "; "
						+ rs.getString("CITY"));

		} catch (Exception e) {
			System.err.println(e);
		} finally {
			con.close();
		}
	}

}
