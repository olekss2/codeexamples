package additional;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CSVFile {
	ReadFile file = new ReadFile();

	public static void main(String[] args) throws Exception {
		CSVFile c = new CSVFile();
		System.out.println(c.csvData().toString());

	}

	public List<Country> csvData() throws Exception {
		file.setFile("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\Files_Project\\Countries\\fileCSV.csv");
		String fileContent = file.getContent();
		List<Country> countryList = new ArrayList<>();
		Country country = new Country();
		List<String> lineList = new ArrayList<>();
		List<String> fieldList = new ArrayList<>();
		List<String> columnList = new ArrayList<>();
		String delimiter = ";";
		String parameter;

		BufferedReader reader = new BufferedReader(new StringReader(fileContent));
		String fileLine = reader.readLine();
		while (fileLine != null) {
			lineList.add(fileLine);
			fileLine = reader.readLine();
		}
		StringTokenizer columnNames = new StringTokenizer(lineList.get(0), delimiter);
		while (columnNames.hasMoreTokens()) {
			columnList.add(columnNames.nextToken());
		}

		for (int i = 1; i < lineList.size(); i++) {
			StringTokenizer st = new StringTokenizer(lineList.get(i), delimiter);
			while (st.hasMoreTokens()) {
				fieldList.add(st.nextToken());
			}

			for (int j = 0; j < columnList.size(); j++) {
				parameter = (String) columnList.get(j);

				switch (parameter) {
				case "ID":
					country.setId(Integer.valueOf((String) fieldList.get(j)));
					break;

				case "Name":
					country.setName((String) fieldList.get(j));
					break;

				case "CapitalCity":
					country.setCapital((String) fieldList.get(j));
					break;

				case "Currency":
					country.setCurrency((String) fieldList.get(j));
					break;

				case "GDP":
					country.setGdp(Double.valueOf((String) fieldList.get(j)));
					break;
				}
			}

			countryList.add(country);
			fieldList.clear();
			country = new Country();
		}
		return countryList;

	}
}