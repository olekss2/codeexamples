package additional;

import java.util.NoSuchElementException;

public class TheClassWithException implements InterfaceFrameWork{

	@Override
	public void method(){
		throw new NoSuchElementException();
	}

}
