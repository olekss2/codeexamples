package additional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TestLoop {

	public static void main(String[] args) {

		List<Book> bookSet = new ArrayList<Book>();
		bookSet.add(new Book("B", "J.R.Rowling", "Harry Potter", 1997));
		bookSet.add(new Book("A", "A.Deglavs", "Rīga", 1963));
		bookSet.add(new Book("B", "Tolkien", "Lord of The Rings", 1964));
		bookSet.add(new Book("A", "Fitzgerald", "Great Gatsby", 1928));
		bookSet.add(new Book("C", "Fitzgerald", "Great Gatsby 2", 1942));
		bookSet.add(new Book("C", "Fitzgerald", "Great Gatsby 3", 1942));
		bookSet.add(new Book("C", "Fitzgerald", "Great Gatsby 4", 1944));
		bookSet.sort(Comparator.naturalOrder());

		Map<String, Integer> categories = new TreeMap<String, Integer>();

		// define previous book
		Book previous = null;
		int count = 1;

		if (bookSet.size() == 0)
			return;

		
		/*A
		 *A 
		 *C 
		 *A
		 *A
		 *C
		 *
		 *Map<ElementSortBy,Counter> map
		 *
		 * 
		 * for(.....Orders... -> a){
		 * 
		 * check if a is not in the map
		 * 
		 * for(.....Orders... -> b, where b == a)
		 * count += 1;
		 * }
		 * add a + count to map
		 * }
		 */
		
		
		for (Book book : bookSet) {

			if (previous == null) {
				previous = book;
				continue;
			}

			if (previous.getCategory() == book.getCategory()) {
				count += 1;
				continue;
			}

			categories.put(previous.getCategory(), count);
			count = 1;
			previous = book;
		}

		categories.put(previous.getCategory(), count);
		System.out.println(categories);
	}

}

class Book implements Comparable<Book> {
	private String category;
	private String author;
	private String name;
	private int yearPublished = 0;

	@Override
	public int compareTo(Book book) {
		return this.getCategory().compareTo(book.getCategory());
	}

	public Book(String category, String author, String name, int year) {
		// TODO Auto-generated constructor stub
		this.category = category;
		this.author = author;
		this.name = name;
		this.yearPublished = year;
	}

	public String getCategory() {
		return this.category;
	}

	@Override
	public String toString() {
		return this.author + this.name;
	}
}