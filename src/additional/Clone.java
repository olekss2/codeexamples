package additional;

public class Clone {

	public static void main(String[] args) throws Exception {

		ClassThatCanBeCloned object1 = new ClassThatCanBeCloned("TextABCD");
		ClassThatCanBeCloned object2 = (ClassThatCanBeCloned) object1.clone();

		object2.setText("Textfrom2");
		System.out.println(object1.getText());
		System.out.println(object2.getText());

	}

}

class ClassThatCanBeCloned implements Cloneable{

	String text;

	public String getText() {
		return text;
	}

	public ClassThatCanBeCloned(String text) {
		this.text = text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

}
