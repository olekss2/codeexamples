package additional;

public class TestClones {

	public static void main(String[] args) {
		Cat cat1 = new Cat();
		cat1.color = "Black";

		try {
			Cat cat2 = (Cat) cat1.clone();
			cat2.color = "Black";
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

class Cat implements Cloneable {

	public String color;

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

}
