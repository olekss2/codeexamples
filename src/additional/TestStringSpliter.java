package additional;

import java.io.FileReader;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class TestStringSpliter {

	private BufferedReader input;

	public static void main(String[] args) {
		// Enter data using BufferReader
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// Reading data using readLine
		try {
			String filepath = reader.readLine();
			String delim = reader.readLine();

			TestStringSpliter obj = new TestStringSpliter();
			List<Computer> listComp = obj.getComputersFromFile(filepath, delim);
			System.out.println(listComp.toString());

		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("Error occured!");
		}
	}

	private List<Computer> getComputersFromFile(String filepath, String delimiter) {
		File computersFile = new File(filepath);
		List<Computer> computersList = new ArrayList<Computer>();

		try {
			input = new BufferedReader(new FileReader(computersFile));
			String lineStr = "\n";
			boolean firstLine = true;
			String[] columns = new String[4];

			while ((lineStr = input.readLine()) != null) {

				StringTokenizer line = new StringTokenizer(lineStr, delimiter);
				if (firstLine == true) {
					columns[0] = (String) line.nextElement();
					columns[1] = (String) line.nextElement();
					columns[2] = (String) line.nextElement();
					columns[3] = (String) line.nextElement();
					firstLine = false;
					continue;
				}

				Computer computer = new Computer();

				setValue(columns[0], line, computer);
				setValue(columns[1], line, computer);
				setValue(columns[2], line, computer);
				setValue(columns[3], line, computer);
				computersList.add(computer);
			}
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("Error occured!");
		}
		return computersList;
	}

	private void setValue(String fieldName, StringTokenizer line, Computer computer) {
		switch (fieldName) {
		case "Brand":
			computer.setBrand((String) line.nextElement());
			break;
		case "SerialNumber":
			computer.setSerial(Integer.valueOf((String) line.nextElement()));
			break;
		case "Model":
			computer.setModel((String) line.nextElement());
			break;
		case "Year":
			computer.setYear(Integer.valueOf((String) line.nextElement()));
			break;
		}
	}

}

class Computer {
	private int year;
	private int serialNumber;
	private String brand;
	private String model;

	public void setYear(int year) {
		this.year = year;
	}

	public void setSerial(int serial) {
		this.serialNumber = serial;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return this.year;
	}

	public int getSearial() {
		return this.serialNumber;
	}

	public String getBrand() {
		return this.brand;
	}

	public String getModel() {
		return this.model;
	}

}
