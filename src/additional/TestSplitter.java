package additional;

import java.util.StringTokenizer;

public class TestSplitter {

	public static void main(String[] args) {
		String text = "1.Value.Name.Surname.";
		String[] textArray = new String[4];

		StringTokenizer line = new StringTokenizer(text, ".");
		for (int i = 0; i < 4; i++) {
			textArray[i] = (String) line.nextElement();
		}

		System.out.println(textArray.toString());

	}

}
