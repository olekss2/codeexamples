package additional;

public class Cycle {

	static String array[] = { "A", "B", "C" };

	public static void main(String[] args) {

		for (String s : array) {

			if (s == "B")
				continue;

			System.out.println(s);
		}

	}

}
