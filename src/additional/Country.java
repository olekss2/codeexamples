package additional;

public class Country {

    private int id;
    private String name;
    private String capitalCity;
    private String currency;
    private double gdp;

    public Country() {
    }

    public Country(int id, String name, String capital, String currency, int gdp) {
        this.id = id;
        this.name = name;
        this.capitalCity = capital;
        this.currency = currency;
        this.gdp = gdp;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capitalCity;
    }

    public String getCurrency() {
        return currency;
    }

    public double getGdp() {
        return gdp;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapital(String capital) {
        this.capitalCity = capital;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setGdp(double gdp) {
        this.gdp = gdp;
    }

    public String toString() {
        return this.id + ": " + this.name + " [" + this.capitalCity + "; " + this.currency + "; " + this.gdp + "]";
    }
}