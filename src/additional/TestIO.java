package additional;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public class TestIO {

	public static void main(String[] args) throws Exception {
//		System.out.println("simple message");
//		System.err.println("error message");
//		int i = System.in.read();// returns ASCII code of 1st character
//		System.out.println((char) i);// will print the character

//		try {
//			FileOutputStream fout = new FileOutputStream("/Users/olekss/Documents/FileTest.txt");
//
//			String text = "Here is the tex2";
//
////			for (char c : text.toCharArray())
////				fout.write((int) c);
//
//			fout.write(text.getBytes());
//
//			fout.close();
//			System.out.println("success...");
//		} catch (Exception e) {
//			System.out.println(e);
//		}

		try {
			FileInputStream fin = new FileInputStream("/Users/olekss/Documents/FileTest.txt");
			byte[] value = fin.readAllBytes();
			System.out.print(new String(value));

			fin.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
