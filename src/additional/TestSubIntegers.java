package additional;

public class TestSubIntegers {

	public static void main(String[] args) {

		SubIntegers.start = 3; // From which digit to start
		SubIntegers.end = 5; // At which digit to finish
		SubIntegers.getSubInteger(78472347); // Get the substring
	}

}

class SubIntegers {

	public static int start, end; // Declare the variables to use

	public static void getSubInteger(int integer) {

		int result; // the variable to be used as the result
		int len = getIntLen(integer); // length of the input integer

		/*
		 * Here below we want to trim the integer (first start - 1 digits) at the
		 * beginning. The idea is that we subtract it from the actual number. If we get
		 * the remaining part of the integer after dividing it by 10^(length of the
		 * integer - start position + 1), we actually get rid of the starting digits.
		 * 
		 * To trim the ending part, we will use the integer which was trimmed at the
		 * beginning. We use the length of it. At first, we want to get value of the
		 * part to be trimmed from the end of the integer. Again we use the modulo to
		 * get it. In this case we need to divide by 10 in the power of (new length -
		 * (end - start + 1). This will get us the value of the part to be trimmed. To
		 * remove this final part from the current integer (the initial one trimmed from
		 * the beginning), we need to subtract the value to be trimmed from current
		 * integer.This will result in the integer which will have the zeros at the end.
		 * So, for now we just only need to get rid of that zeros. This can be done if
		 * we divide the current integer (the initial one trimmed from the beginning and
		 * with the zeros at end - at the place which needs to be trimmed from the end)
		 * by 10 in the power of the length of the ending part to be trimmed. At the
		 * result, we will have the integer trimmed from the initial position to start
		 * (not included) and from end position (not included) till the end.
		 * 
		 * Example. integer = 32718332. If we want to start, ex., at 3rd position, it
		 * means that we need to get rid of preceding 32 or subtract 32000000 from the
		 * integer (the same). To get the value of 2 preceding digits, we need to divide
		 * the integer by 10^(length - start - 1) = 10^(8 - 3 + 1) = 10^6 = 1000000. If
		 * we divide integer = 32718332 by 1000000 we get 32 (which are 2 preceding
		 * digits of the integer number provided). Then if we subtract 32 * 1000000 from
		 * 32718332 we get 718332.So, we got the sub-integer starting at 3 position.
		 * These steps can be simplified, if we simply use the modulo of the integer to
		 * 10^6 (get's the remaining part of the division of integer by 10^6).
		 * 
		 * The similar way, we can trim the ending part. If we want to trim, ex., last 2
		 * (starting after 6th position - end) digits of the integer number. At first,
		 * we get the value of those 2 (new length - (end - start + 1) = 6 - (6 - 3 + 1)
		 * = 6 - 4 = 2 ) digits. To get it, we need to get the remaining part by
		 * dividing the integer by 10^(2) and we get 32. After that, we subtract the
		 * value from 718332. The we get 718332 -32 = 718300. So, the only thing left is
		 * to get rid of the last 2 zeros. Thus, we just need to divide 718300 by 10^2
		 * and we get 7183.
		 */
		result = integer % pow(10, (len - start + 1));

		len = getIntLen(result);
		int lenEnd = len - (end - start + 1);
		int endPart = result % pow(10, lenEnd);

		result = (result - endPart) / pow(10, lenEnd);

		System.out.println(result);

	}

	private static int pow(int integer, int power) {

		int result = integer;

		for (int i = 1; i < power; i++) {
			result *= integer;
		}
		return result;

	}

	private static int getIntLen(int integer) {

		int result = 1;
		while (true) {
			if (integer / pow(10, result) == 0)
				return result;
			result += 1;
		}

	}

}
