package exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Example {

	public static void main(String[] args) {
		PictureBuiler picBuilder = new PictureBuiler();
		picBuilder.run();
	}

}

class PictureBuiler {

	private int z, n;
	private String[] lines;
	private String lineBreak = "";

	public void run() {

		try {
			if (!this.read()) {
				System.out.println("Input z = " + this.z + ", n = " + this.n + " is incorrect!");
				return;
			}
			this.draw();
		} catch (IOException e) {
			System.out.println("Input/Output exception occured");
		}
//		} catch (InccorrectInputData e) {
//			e.printStackTrace();
//		}
	}

	private boolean read() throws IOException { /// ,InccorrectInputData {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Z:");
		this.z = Integer.parseInt(reader.readLine());
		System.out.println("Enter N:");
		this.n = Integer.parseInt(reader.readLine());

		if (this.checkValidaty() == false)
			return false;
		this.lines = new String[this.z];
		return true;

	}

	private boolean checkValidaty() { //// throws InccorrectInputData {

		if (!(this.z > 0 && this.z < 20) || !(n > 0 && n < 30))
//			throw new InccorrectInputData(this.z, this.n);
			return false;
		else
			return true;

	}

	public void draw() {
		for (int i = 0; i < this.z; i++) {
			this.lineBreak += " ";
		}

		int currentNumber = this.z;

		for (int i = 0; i < this.z; i++) {
			this.lines[i] = this.lineBreak;

			for (int j = 0; j < currentNumber; j++) {
				this.lines[i] += "+ ";
			}
			currentNumber -= 1;
			this.lineBreak += " ";

		}

		for (int i = this.z - 1; i >= 0; i--) {
			System.out.println(lines[i]);
		}

	}

}

//class InccorrectInputData extends Exception {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	int z, n;
//
//	public InccorrectInputData(int z, int n) {
//		this.z = z;
//		this.n = n;
//	}
//
//	@Override
//	public void printStackTrace() {
//		System.out.println("Input z = " + this.z + ", n = " + this.n + " is incorrect!");
//	}
//
//}