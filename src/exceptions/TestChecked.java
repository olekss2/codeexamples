package exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class TestChecked {

	public String color;
	private static List<Exception> exception = new ArrayList<Exception>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			TestChecked instance = new TestChecked();
			instance.doSomething();

			int i = 0;
			String lines[] = { "AAA", "BBB", "CCC" };
			while (i < 4) {

				System.out.println(lines[i]);
				i++;
			}
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			exception.add(e);
		}

		try {
			TestChecked object = getObject();
			object.color = "red";
		} catch (Exception e) {
			exception.add(e);
		}

		try {

			TestChecked instance = new TestChecked();
			instance.doSomething();

		} catch (FileNotFoundException e) {
			// TODO: handle exception
			exception.add(e);
		}

		for (int i = 0; i < exception.size(); i++) {

			exception.get(i).printStackTrace();
		}
	}

	private void doSomething() throws FileNotFoundException {
		FileInputStream file = new FileInputStream(".src/file.txt");
	}

	private static TestChecked getObject() {
		return null;
	}

}
